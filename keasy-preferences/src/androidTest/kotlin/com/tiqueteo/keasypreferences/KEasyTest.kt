/** <!-- Documentation for: com.tiqueteo.keasypreferences.KEasyTest on 29/05/2017 -->
 *
 * @author Aran Moncusí Ramírez
 */
package com.tiqueteo.keasypreferences

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.test.InstrumentationRegistry
import android.support.test.filters.LargeTest
import android.support.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


//~ Constants ==========================================================================================================

val setStringDefaultValues = setOf("default", "null", "override")

internal val initNormalProp: AllTypesOfPreferences.() -> Unit = {
	booleanField = true
	intField = 123
	longField = 111L
	floatField = 0.111F
	stringField = "Supreme"
	setStringField = setOf("one", "two", "three")
	nullStringField = "none null"
}

internal val initNormalCustomProp: AllTypesOfPreferencesByCustomName.() -> Unit = {
	booleanField = true
	intField = 123
	longField = 111L
	floatField = 0.111F
	stringField = "Supreme"
	setStringField = setOf("one", "two", "three")
	nullStringField = "none null"
}

//~ Functions ==========================================================================================================

//~ Extensions =========================================================================================================

//~ Annotations ========================================================================================================

//~ Interfaces =========================================================================================================

internal abstract class CustomPref(context: Context):
		KEasyPreferences(context, "anonymous_test_class_with_interface")
{
	abstract var one: String
	abstract var two: Int
}


//~ Enums ==============================================================================================================

//~ Data Classes =======================================================================================================

// Example class for KEasy preferences

internal open class AllTypesOfPreferences(context: Context, name: String? = "all_types_of_preferences_test"):
		KEasyPreferences(context, name)
{
	var booleanField: Boolean by KEasyBoolean(false)

	var intField: Int by KEasyInt(-1)

	var longField: Long by KEasyLong(-1L)

	var floatField: Float by KEasyFloat(-1.0F)

	var stringField: String by KEasyString("default")

	var setStringField: Set<String> by KEasyStringSet(setStringDefaultValues)

	var nullStringField: String? by KEasyNullableString(null)
}

// Test collision names to different classes with same fields names
internal class NoneCollisionProperties(context: Context): AllTypesOfPreferences(context)

internal class CollisionProperties(context: Context): AllTypesOfPreferences(context, null)

internal class AllTypesOfPreferencesByCustomName(context: Context):
		KEasyPreferences(context, "all_types_of_preferences_test")
{
	var booleanField: Boolean by KEasyBoolean(false, key = "boolean")

	var intField: Int by KEasyInt(-1, key = "integer")

	var longField: Long by KEasyLong(-1L, key = "long")

	var floatField: Float by KEasyFloat(-1.0F, key = "float")

	var stringField: String by KEasyString("default", key = "string")

	var setStringField: Set<String> by KEasyStringSet(setOf("default", "null", "override"), key = "stringSet")

	var nullStringField: String? by KEasyNullableString(null, key = "stringNull")
}

//~ Classes ============================================================================================================

@RunWith(AndroidJUnit4::class)
@LargeTest
class KEasyTest
{
	//~ Constants ======================================================================================================

	//~ Values =========================================================================================================

	lateinit var context: Context

	lateinit var defaultPreferences: SharedPreferences

	val preferences = mutableListOf<KEasyPreferences>()

	//~ Properties =====================================================================================================

	//~ Open Methods ===================================================================================================

	//~ Methods ========================================================================================================

	@Before
	fun initFields()
	{
		context = InstrumentationRegistry.getContext()
		defaultPreferences = PreferenceManager.getDefaultSharedPreferences(context)
	}

	@After
	@SuppressLint("CommitPrefEdits")
	fun clearAllPreferences()
	{
		defaultPreferences.edit().clear().apply()
		preferences.map { it.editor.clear().commit() }
	}

	internal fun getAnonymousClass(context: Context): CustomPref
	{
		val anonymous =  object : CustomPref(context)
		{
			override var one: String by KEasyString("one")
			override var two: Int by KEasyInt(-128)
		}

		return anonymous.apply { this@KEasyTest.preferences.add(this) }
	}

	//~ Test Methods ===================================================================================================

	@Test
	fun testAnonymousInstances()
	{
		val oneAnon = getAnonymousClass(context)
		val twoAnon = getAnonymousClass(context)

		assertEquals("one", oneAnon.one)
		assertEquals(-128, oneAnon.two)

		twoAnon.editAndCommit {
			one = "Ho!"
			two = 123
		}

		assertEquals(oneAnon.one, twoAnon.one)
		assertEquals(oneAnon.two, twoAnon.two)
	}

	@Test
	fun saveValues()
	{
		val prop = AllTypesOfPreferencesByCustomName(context)

		prop.editAndCommit(initNormalCustomProp)

		assertEquals(true, prop.booleanField)
		assertEquals(123, prop.intField)
		assertEquals(111L, prop.longField)
		assertEquals(0.111F, prop.floatField)
		assertEquals("Supreme", prop.stringField)
		assertEquals(setOf("one", "two", "three"), prop.setStringField)
		assertEquals("none null", prop.nullStringField)

		preferences.add(prop)
	}

	@Test
	fun defaultValuesTest()
	{
		// By default name ("all_types...")
		val defaultTest = AllTypesOfPreferences(context, "unique_name_for_not_share_any_property_1231234123")

		assertEquals(false, defaultTest.booleanField)
		assertEquals(-1, defaultTest.intField)
		assertEquals(-1L, defaultTest.longField)
		assertEquals(-1.0F, defaultTest.floatField)
		assertEquals("default", defaultTest.stringField)
		assertEquals(setStringDefaultValues, defaultTest.setStringField)
		assertEquals(null, defaultTest.nullStringField)

		preferences.add(defaultTest)
	}

	@Test
	fun checkCollisionValues()
	{
		// By class name
		val (allOne, allTwo) = arrayOf(AllTypesOfPreferences(context), NoneCollisionProperties(context))
		val difference = CollisionProperties(context)

		allOne.editAndCommit(initNormalProp)

		// Check has saved values
		assertEquals(true, allOne.booleanField)
		assertEquals(123, allOne.intField)
		assertEquals(111L, allOne.longField)
		assertEquals(0.111F, allOne.floatField)
		assertEquals("Supreme", allOne.stringField)
		assertEquals(setOf("one", "two", "three"), allOne.setStringField)
		assertEquals("none null", allOne.nullStringField)

		// Same class equality
		assertEquals(allOne.booleanField, allTwo.booleanField)
		assertEquals(allOne.intField, allTwo.intField)
		assertEquals(allOne.longField, allTwo.longField)
		assertEquals(allOne.floatField, allTwo.floatField)
		assertEquals(allOne.stringField, allTwo.stringField)
		assertEquals(allOne.setStringField, allTwo.setStringField)
		assertEquals(allOne.nullStringField, allTwo.nullStringField)

		// Cannot modified values!
		assertEquals(false, difference.booleanField)
		assertEquals(-1, difference.intField)
		assertEquals(-1L, difference.longField)
		assertEquals(-1.0F, difference.floatField)
		assertEquals("default", difference.stringField)
		assertEquals(setStringDefaultValues, difference.setStringField)
		assertEquals(null, difference.nullStringField)

		// Other class not equality
		assertNotEquals(allTwo.booleanField, difference.booleanField)
		assertNotEquals(allTwo.intField, difference.intField)
		assertNotEquals(allTwo.longField, difference.longField)
		assertNotEquals(allTwo.floatField, difference.floatField)
		assertNotEquals(allTwo.stringField, difference.stringField)
		assertNotEquals(allTwo.setStringField, difference.setStringField)
		assertNotEquals(allTwo.nullStringField, difference.nullStringField)

		preferences.addAll(listOf(allOne, allTwo, difference))
	}

	//~ Operators ======================================================================================================
}

//~ Sealed Classes =====================================================================================================

//~ Objects ============================================================================================================
