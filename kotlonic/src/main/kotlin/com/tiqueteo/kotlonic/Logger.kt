/** <!-- Documentation for: com.tiqueteo.multidevice.verifier.util.Logger on 24/05/2017 -->
 *
 * @author Aran Moncusí Ramírez
 */

@file:Suppress("NOTHING_TO_INLINE")
package com.tiqueteo.kotlonic

import android.util.Log


//~ Constants ==========================================================================================================

//~ Functions ==========================================================================================================

//~ Extensions =========================================================================================================

//:: Verbose :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

val CustomLogger.verbose
	get() = logLevel <= Log.VERBOSE

@JvmOverloads
inline fun CustomLogger.verbose(message: String, e: Throwable? = null, safe: Boolean = false): Unit
{
	if(verbose && (!safe || Log.isLoggable(loggerTag, Log.VERBOSE)))
		if (e == null) Log.v(loggerTag, message) else Log.v(loggerTag, message, e)
}

inline fun CustomLogger.verbose(msg: () -> Any?): Unit
{
	if(verbose)
		Log.v(loggerTag, msg()?.toString() ?: "null")
}

inline fun CustomLogger.isVerbose(msg: () -> Any?): Unit
{
	if(verbose && Log.isLoggable(loggerTag, Log.VERBOSE)) Log.v(loggerTag, msg()?.toString() ?: "null")
}

//:: Debug :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

val CustomLogger.debug
	get() = logLevel <= Log.DEBUG

@JvmOverloads
inline fun CustomLogger.debug(message: String, e: Throwable? = null, safe: Boolean = false): Unit
{
	if(debug && (!safe || Log.isLoggable(loggerTag, Log.DEBUG)))
		if (e == null) Log.d(loggerTag, message) else Log.d(loggerTag, message, e)
}

inline fun CustomLogger.debug(msg: () -> Any?): Unit
{
	if (debug)
		Log.d(loggerTag, msg()?.toString() ?: "null")
}

inline fun CustomLogger.isDebug(msg: () -> Any?): Unit
{
	if(debug && Log.isLoggable(loggerTag, Log.DEBUG)) Log.d(loggerTag, msg()?.toString() ?: "null")
}

//:: Info ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

val CustomLogger.info
	get() = logLevel <= Log.INFO

@JvmOverloads
inline fun CustomLogger.info(message: String, e: Throwable? = null, safe: Boolean = false): Unit
{
	if(info && (!safe || Log.isLoggable(loggerTag, Log.INFO)))
		if (e == null) Log.i(loggerTag, message) else Log.i(loggerTag, message, e)
}

inline fun CustomLogger.info(msg: () -> Any?): Unit
{
	if (debug)
		Log.i(loggerTag, msg()?.toString() ?: "null")
}

inline fun CustomLogger.isInfo(msg: () -> Any?): Unit
{
	if(debug && Log.isLoggable(loggerTag, Log.INFO)) Log.i(loggerTag, msg()?.toString() ?: "null")
}

//:: Warning :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

val CustomLogger.warn
	get() = logLevel <= Log.WARN

@JvmOverloads
inline fun CustomLogger.warn(message: String, e: Throwable? = null, safe: Boolean = false): Unit
{
	if(warn && (!safe || Log.isLoggable(loggerTag, Log.WARN)))
		if (e == null) Log.w(loggerTag, message) else Log.w(loggerTag, message, e)
}

inline fun CustomLogger.warn(msg: () -> Any?): Unit
{
	if(!warn) return

	val value = msg()
	when (value)
	{
		is Throwable	-> Log.w(loggerTag, value)
		else			-> Log.w(loggerTag, value?.toString() ?: "null");
	}
}

inline fun CustomLogger.isWarn(msg: () -> Any?): Unit
{
	if(warn && Log.isLoggable(loggerTag, Log.WARN)) Log.w(loggerTag, msg()?.toString() ?: "null")
}

//:: Error :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

val CustomLogger.error
	get() = logLevel <= Log.ERROR

@JvmOverloads
inline fun CustomLogger.error(message: String, e: Throwable? = null, safe: Boolean = false): Unit
{
	if(error && (!safe || Log.isLoggable(loggerTag, Log.ERROR)))
		if (e == null) Log.e(loggerTag, message) else Log.e(loggerTag, message, e)
}

inline fun CustomLogger.error(msg: () -> Any?): Unit
{
	if (error)
		Log.e(loggerTag, msg()?.toString() ?: "null")
}

inline fun CustomLogger.isError(msg: () -> Any?): Unit
{
	if(error && Log.isLoggable(loggerTag, Log.ERROR)) Log.e(loggerTag, msg()?.toString() ?: "null")
}

//:: WTF :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

inline fun CustomLogger.wtf(message: String, e: Throwable? = null): Unit
{
	if (e == null) Log.wtf(loggerTag, message) else Log.wtf(loggerTag, message, e)
}

inline fun CustomLogger.wtf(msg: () -> Any?): Unit
{
	Log.wtf(loggerTag, msg()?.toString() ?: "null")
}

//~ Annotations ========================================================================================================

//~ Interfaces =========================================================================================================

interface CustomLogger
{
	val loggerTag: String
		get() = javaClass.name.safeSubstring(0..23)

	val logLevel: Int
		get() = 0
}

//~ Enums ==============================================================================================================

//~ Data Classes =======================================================================================================

//~ Classes ============================================================================================================

//~ Sealed Classes =====================================================================================================

//~ Objects ============================================================================================================