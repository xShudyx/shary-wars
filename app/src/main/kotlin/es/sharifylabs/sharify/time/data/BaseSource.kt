package es.sharifylabs.sharify.time.data

/** <!-- Documentation for: es.sharifylabs.sharify.time.data.BaseSource on 20/09/18 -->
 *
 * Base of all Sources.
 *
 *
 *
 * @author Aran Moncusí Ramírez
 */
interface BaseSource
{
    //~ Constants ======================================================================================================

    //~ Values =========================================================================================================

    //~ Properties =====================================================================================================

    //~ Methods ========================================================================================================

    /**
     * Returns true if this resource has been disposed.
     *
     * Example: If any implemented method will be calling the service and waiting the response, the returned value will
     * be `true`
     *
     * @return true if this resource has been disposed
     */
    fun isDisposed(): Boolean

    /**
     * Dispose the resource, the operation should be idempotent.
     * When dispose the resource, it will kill all threads in background if has any one.
     */
    fun dispose()

    //~ Operators ======================================================================================================
}
