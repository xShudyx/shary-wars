/** <!-- Documentation for: com.tiqueteo.keasypreferences.KEasyPreferences on 25/05/2017 -->
 *
 * @author Aran Moncusí Ramírez
 */
@file:Suppress("AddVarianceModifier", "unused")
package com.tiqueteo.keasypreferences

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.tiqueteo.kotlonic.verbose
import kotlin.properties.ReadWriteProperty


//~ Constants ==========================================================================================================

//~ Functions ==========================================================================================================

private fun checkSetString(value: Any): Boolean = value is Set<*> && value.size > 0 && value.first() is String

private fun checkEmptySet(value: Any): Boolean = value is Set<*> && value.isEmpty()

//~ Extensions =========================================================================================================

/**
 * Edit instance inside lambda in scope of self and when ended, applying changes using `commit()` method
 */
inline fun <T: KEasyPreferences> T.editAndCommit(fn: T.() -> Unit): T
{
	synchronized(this::class) { this.fn() }
    this.commit()
	return this
}

/**
 * Edit instance inside lambda in scope of self and when ended, applying changes using `apply()` method
 */
inline fun <T: KEasyPreferences> T.editAndApply(fn: T.() -> Unit): T
{
	synchronized(this::class) { this.fn() }
    this.apply()
	return this
}

//~ Annotations ========================================================================================================

//~ Interfaces =========================================================================================================

interface KEasySharedProperty<C: KEasyPreferences, T>: ReadWriteProperty<C, T>
{
	/**
	 * Get saved value in SharedPreferences
	 */
	fun getSavedValue(pref: SharedPreferences, key: String, default: T): T

	/**
	 * Save value in SharedProperties
	 */
	fun saveValue(editor: SharedPreferences.Editor, key: String, value: T): Unit
}

/**
 * Basic interface to set persistence methods for save state of values.
 */
interface KEasy
{
	fun commit(): Boolean

	fun apply()
}

//~ Enums ==============================================================================================================

//~ Data Classes =======================================================================================================

//~ Classes ============================================================================================================

/**
 * Superclass of data classes when implementing "SharedPreferences". Provide a 'proxy' to work under the hood using
 * `SharedPreferences` persistence
 *
 * @param context Android context
 * @param name Custom name of shared preferences or null (if set null, use qualified name of class)
 * @param mode [SharedPreferences] Mode. See: [Context.MODE_PRIVATE]. Other modes has ben deprecated, and not
 * recommend they use
 */
@SuppressLint("CommitPrefEdits")
abstract class KEasyPreferences
	@JvmOverloads constructor(context: Context, name: String? = null, mode: Int = Context.MODE_PRIVATE):
		KEasyLogger, KEasy
{
	//~ Constants ======================================================================================================

	val preferences: SharedPreferences by lazy { context.getSharedPreferences(getPropertyName(name), mode) }

	val editor: SharedPreferences.Editor by lazy { preferences.edit() }

	//~ Values =========================================================================================================

	//~ Properties =====================================================================================================

	//~ Constructors ===================================================================================================

	//~ Open Methods ===================================================================================================

	//~ Methods ========================================================================================================

	override fun commit(): Boolean = editor.commit()

	override fun apply()
	{
		editor.apply()
	}

	internal open fun getPropertyName(name: String?): String
	{
		val preferencesName = name ?: this::class.java.`package`.name + "." + (this::class.simpleName ?:
				throw IllegalStateException("Cannot extends KEasyPreferences and not define a name in anonymous class"))

		verbose { "Instance of preference with name -> $preferencesName" }

		return preferencesName
	}


	//~ Operators ======================================================================================================
}

//~ Sealed Classes =====================================================================================================

//~ Objects ============================================================================================================
