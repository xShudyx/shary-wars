/** <!-- Documentation for: com.tiqueteo.keasypreferences.KEasyProperty on 26/05/2017 -->
 *
 * @author Aran Moncusí Ramírez
 */
@file:Suppress("unused")

package com.tiqueteo.keasypreferences

import android.content.SharedPreferences
import java.util.*
import kotlin.reflect.KProperty


//~ Constants ==========================================================================================================

//~ Functions ==========================================================================================================

//~ Extensions =========================================================================================================

//~ Annotations ========================================================================================================

//~ Interfaces =========================================================================================================

//~ Enums ==============================================================================================================

//~ Data Classes =======================================================================================================

//~ Classes ============================================================================================================

/**
 * Save Date Object in KEasy objects
 */
open class KEasyDate<C: KEasyPreferences>(default: Date, key: String? = null):
		KEasyAbstractProperty<C, Date>(default, key)
{
	/**
	 * Get saved value in SharedPreferences
	 */
	override fun getSavedValue(pref: SharedPreferences, key: String, default: Date): Date
	{
		return Date(pref.getLong(key, default.time))
	}

	/**
	 * Save value in SharedProperties
	 */
	override fun saveValue(editor: SharedPreferences.Editor, key: String, value: Date)
	{
		editor.putLong(key, value.time)
	}
}

/**
 * Save Booleans in KEasy objects
 */
open class KEasyBoolean<C: KEasyPreferences>(default: Boolean, key: String? = null):
		KEasyAbstractProperty<C, Boolean>(default, key)
{
	override fun getSavedValue(pref: SharedPreferences, key: String, default: Boolean): Boolean
	{
		return pref.getBoolean(key, default)
	}

	override fun saveValue(editor: SharedPreferences.Editor, key: String, value: Boolean)
	{
		editor.putBoolean(key, value)
	}
}

open class KEasyFloat<C: KEasyPreferences>(default: Float, key: String? = null):
		KEasyAbstractProperty<C, Float>(default, key)
{
	override fun getSavedValue(pref: SharedPreferences, key: String, default: Float): Float
	{
		return pref.getFloat(key, default)
	}

	override fun saveValue(editor: SharedPreferences.Editor, key: String, value: Float)
	{
		editor.putFloat(key, value)
	}
}

open class KEasyLong<C: KEasyPreferences>(default: Long, key: String? = null):
		KEasyAbstractProperty<C, Long>(default, key)
{
	override fun getSavedValue(pref: SharedPreferences, key: String, default: Long): Long
	{
		return pref.getLong(key, default)
	}

	override fun saveValue(editor: SharedPreferences.Editor, key: String, value: Long)
	{
		editor.putLong(key, value)
	}
}

open class KEasyInt<C: KEasyPreferences>(default: Int, key: String? = null):
		KEasyAbstractProperty<C, Int>(default, key)
{
	override fun getSavedValue(pref: SharedPreferences, key: String, default: Int): Int
	{
		return pref.getInt(key, default)
	}

	override fun saveValue(editor: SharedPreferences.Editor, key: String, value: Int)
	{
		editor.putInt(key, value)
	}
}

open class KEasyStringSet<C: KEasyPreferences>(default: Set<String>, key: String? = null):
		KEasyAbstractProperty<C, Set<String>>(default, key)
{
	override fun getSavedValue(pref: SharedPreferences, key: String, default: Set<String>): Set<String>
	{
		return pref.getStringSet(key, default)
	}

	override fun saveValue(editor: SharedPreferences.Editor, key: String, value: Set<String>)
	{
		editor.putStringSet(key, value)
	}
}

open class KEasyString<C: KEasyPreferences>(default: String, key: String? = null):
		KEasyAbstractProperty<C, String>(default, key)
{
	//~ Methods ========================================================================================================

	override fun getSavedValue(pref: SharedPreferences, key: String, default: String): String
	{
		return pref.getString(key, default)!!
	}

	override fun saveValue(editor: SharedPreferences.Editor, key: String, value: String)
	{
		editor.putString(key, value)
	}

	//~ Operators ======================================================================================================
}

open class KEasyNullableString<C: KEasyPreferences>(default: String?, key: String? = null):
		KEasyAbstractProperty<C, String?>(default, key)
{
	//~ Methods ========================================================================================================

	override fun getSavedValue(pref: SharedPreferences, key: String, default: String?): String?
	{
		return pref.getString(key, default)
	}

	override fun saveValue(editor: SharedPreferences.Editor, key: String, value: String?)
	{
		editor.putString(key, value)
	}

	//~ Operators ======================================================================================================
}

//~ Abstract Classes ===================================================================================================

/**
 * Abstract class for simplify get/save values.
 */
abstract class KEasyAbstractProperty<C: KEasyPreferences, T>(open val default: T, val key: String? = null):
		KEasySharedProperty<C, T>
{
	//~ Methods ========================================================================================================

	override fun getValue(thisRef: C, property: KProperty<*>): T
	{
		return getSavedValue(thisRef.preferences, key ?: property.name, default)
	}

	override fun setValue(thisRef: C, property: KProperty<*>, value: T): Unit
	{
		saveValue(thisRef.editor, key ?: property.name, value)
	}

	//~ Operators ======================================================================================================
}

//~ Sealed Classes =====================================================================================================

//~ Objects ============================================================================================================
