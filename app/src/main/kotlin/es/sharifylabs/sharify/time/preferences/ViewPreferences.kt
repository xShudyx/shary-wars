package es.sharifylabs.sharify.time.preferences

/** <!-- Documentation for: es.sharifylabs.sharify.time.preferences.ViewPreferences on 20/09/18 -->
 *
 * User status in application
 *
 * @author Aran Moncusí Ramírez
 */
interface ViewPreferences
{
    //~ Constants ======================================================================================================

    //~ Values =========================================================================================================

    /**
     * If view show vehicle price or not. Be `true` by default.
     */
    var showPrice: Boolean

    //~ Properties =====================================================================================================

    //~ Methods ========================================================================================================

    //~ Operators ======================================================================================================
}
